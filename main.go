package main

import (
	"fmt"
	"gitlab.com/dangerous-things/nofunc"
)

func main() {
	something := nofunc.Maybe{}.OfSome(42)
	nothing := nofunc.Maybe{}.OfNone()

	fmt.Println(nothing)
	fmt.Println(nothing.ErrorMessage())
	
	fmt.Println(something)
	fmt.Println(something.Value())

	if something.IsNone() {
		fmt.Println("something is None")
	}

	if something.IsSome() {
		fmt.Println("something is Some")
	}

	// nofunc.Maybe{}.FromNullable returns an empty interface{}
	// then you need to cast the returned value
	bob := nofunc.Maybe{}.FromNullable("Bob Morane")

	switch bob.(type) { // test the interface type of bob and then cast it
	case nofunc.None:
		fmt.Println("😡 None:", bob.(nofunc.None).ErrorMessage())
		fmt.Println("😃 GetOrElse:", bob.(nofunc.None).GetOrElse("John Doe"))
	case nofunc.Some:
		fmt.Println("😃 Some:", bob.(nofunc.Some).Value())
		fmt.Println("😃 GetOrElse:", bob.(nofunc.Some).GetOrElse("John Doe"))
	}

	johnDoe := nofunc.Maybe{}.FromNullable(nil)

	switch johnDoe.(type) {
	case nofunc.None:
		fmt.Println("😡 None:", johnDoe.(nofunc.None).ErrorMessage())
		fmt.Println("😃 GetOrElse:", johnDoe.(nofunc.None).GetOrElse("John Doe"))
	case nofunc.Some:
		fmt.Println("😃 Some:", johnDoe.(nofunc.Some).Value())
		fmt.Println("😃 GetOrElse:", johnDoe.(nofunc.Some).GetOrElse("John Doe"))
	}

	mapOnBob := bob.(nofunc.Some).Map( func(x interface{}) interface{} {
		return "👋 " + x.(string)
	})

	switch mapOnBob.(type) {
	case nofunc.None:
		fmt.Println("😡")
	case nofunc.Some:
		fmt.Println("😃", mapOnBob.(nofunc.Some).Value())

		hello := func(x interface{}) interface{} {
			return "Hello " + x.(string)
		}

		fmt.Println(mapOnBob.(nofunc.Some).Bind(hello)) // Hello 👋 Bob Morane

	}

	mapOnJohnDoe := johnDoe.(nofunc.None).Map( func(x interface{}) interface{} {
		return "👋 " + x.(string)
	})

	switch mapOnJohnDoe.(type) {
	case nofunc.None:
		fmt.Println("😡")

		hello := func(x interface{}) interface{} {
			return "Hello " + x.(string)
		}

		fmt.Println(mapOnJohnDoe.(nofunc.None).Bind(hello)) // <nil>

	case nofunc.Some:
		fmt.Println("😃", mapOnJohnDoe.(nofunc.Some).Value())
	}

	whenGood := func(x interface{}) interface{} {
		fmt.Println(x, "😍")
		return "good"
	}

	whenBad := func(x interface{}) interface{} {
		fmt.Println(x, "🥵")
		return "bad"
	}

	good := nofunc.Either{}.OfRight("🍨")

	good.Cata(whenBad, whenGood)

	bad := nofunc.Either{}.OfLeft("🌶")

	bad.Cata(whenBad, whenGood)

	fmt.Println(good.Value())
	fmt.Println(bad.Value())

}
